import sys
import math
import attr
import logging

MODULE = sys.modules['__main__'].__file__
__version__ = "0.0.1"
DESCRIPTION = MODULE+"""Atrib library example"""

@attr.s(frozen=True) # immutable
class Point:
	x = attr.ib(default=0, convert=int)
	y = attr.ib(default=0, convert=int)


@attr.s(frozen=True) #immutable
class Circle:
	log = logging.getLogger(__name__)
	lastID = 0
	center = attr.ib(default=Point(0, 0))
	radius = attr.ib(default=0, convert=int)
	name = attr.ib() # default set via __init_name(self):
	internalID = attr.ib()
	bogus = attr.ib(default=4)
	private = attr.ib(init=False)

	@radius.validator
	def __validate(self, attribute, value):
		if value < 0:
			raise ValueError("radius negative ("+str(value)+")")
		
	@internalID.default
	def __init_id(self):
		Circle.log.debug("ID initialized")
		Circle.lastID += 1
		return Circle.lastID - 1
	
	@name.default
	def __init_name(self):
		return "Unnamed Circle ("+str(self.center)+", "+str(self.radius)+")"


	def asdict(self):
		return attr.asdict(self,
		                   filter=attr.filters.exclude(attr.fields(Circle).bogus,
		                                               attr.fields(Circle).internalID,
		                                               attr.fields(Circle).private,
		                                               str))
