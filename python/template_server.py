import os

import cherrypy

from cherrypy import log
import logging

class Server:
	lg = logging.getLogger("Server")
	@cherrypy.expose
#	@cherrypy.tools.json_in()
#	@cherrypy.tools.json_out()
	def index(self):
		cherrypy.log("/index requested", context="Server", severity=logging.DEBUG, traceback=False)
		return open(os.path.join(os.path.dirname(os.path.abspath(__file__)), "web/index.html"))

def runServer(port):
	Server.lg.info("Starting server")
	cherrypy.config.update({'server.socket_port': port})
	conf = {
		'/': {
			'tools.sessions.on': True,
			'tools.staticdir.root': os.path.dirname(os.path.abspath(__file__))
			},
		'/static': {
			'tools.staticdir.on': True,
			'tools.staticdir.dir': os.path.join(os.path.dirname(os.path.abspath(__file__)), 'web/static')
			},
		'/data': {
			'tools.staticdir.on': True,
			'tools.staticdir.dir': os.path.join(os.path.dirname(os.path.abspath(__file__)), 'data')
			}
		}
	cherrypy.server.socket_host = '127.0.0.1'
	cherrypy.quickstart(Server(), "/", conf)

if __name__ == "__main__":
    runServer(port=8888)
